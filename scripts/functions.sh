#!/bin/bash

function isDiff() {
    diff=$(git status --porcelain)
    
    if [ "$diff" != '' ]; then
        return 0
    fi

    return 1
}

function isGitRepo() {
    if [ ! -d .git ]; then return 1; fi
    return 0
}

function hasRemote() {
    git remote get-url $1 > /dev/null 2>&1
    return $?
}

function getDate() {
    echo $(date +'%Y-%m-%d %Hh%Mm%Ss')
}

function log() {
    echo "[$(getDate)] $@"
}
