#!/bin/bash
source /opt/vars
source /opt/functions

START_DATE=$(getDate)
FILENAME="dump_${MYSQL_DATABASE}.sql"

function runCron() {
    dump > /tmp/$FILENAME
    if [ "$?" -ne "0" ]; then
        rm -f /tmp/$FILENAME
        log "Dump failed"
    else
        mv /tmp/$FILENAME "${MYSQL_DATABASE}_dump.sql"
        log "Updated to $FILENAME"

        # Checks if this is a git repository
        if ! isGitRepo
        then
            log "Initializing git reposotory..."
            git init > /dev/null
        fi

        if isDiff; then
            git add . > /dev/null
            log "File $FILENAME added to the repo"
            git commit -m "Backed up at $START_DATE" > /dev/null 2>&1
            log "Commited dump"
        else
            log "Everithing already up to date"
        fi
        
        # Validates if origin remote exists
        if ! hasRemote origin
        then
            log "Remote 'origin' does not exist. If you want to backup remotely, please add a remote repository named 'origin'"
        else
            rm -f /tmp/git_push.log
            git push origin master > /tmp/git_push.log 2>&1

            if [ "$?" -ne "0" ]
            then
                log "- Could not push to remote"
                cat /tmp/git_push.log
                log "- End trace"
            else
                log "Pushed to remote"
            fi
        fi

        
    fi
}


cd /dumps
log "---- Job started"
runCron
log "---- Job finished"
