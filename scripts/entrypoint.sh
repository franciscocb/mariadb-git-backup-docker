#!/bin/bash

# Cron

## Setup env
echo "#!/bin/bash
export MYSQL_HOST=$MYSQL_HOST
export MYSQL_USER=$MYSQL_USER
export MYSQL_PASSWORD=$MYSQL_PASSWORD
export MYSQL_DATABASE=$MYSQL_DATABASE" > /opt/vars

git config --global user.email $GIT_EMAIL
git config --global user.name $GIT_USER

cp /privateKey /root/.ssh/privateKey
chmod 600 /root/.ssh/privateKey

CRON=/etc/cron.d/default_backup
echo -e '* * * * * dumpCron >> /var/log/cron.log 2>&1\n' > $CRON
chmod 0644 $CRON
crontab $CRON

cron
# sleep infinity
tail -f /var/log/cron.log
