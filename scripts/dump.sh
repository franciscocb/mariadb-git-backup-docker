#!/bin/bash

mysqldump \
    --compact \
    --host $MYSQL_HOST \
    --user $MYSQL_USER \
    -p$MYSQL_PASSWORD $MYSQL_DATABASE
