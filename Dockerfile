FROM mariadb:latest

RUN apt update -y && apt install -y cron vim git

COPY scripts/dump.sh /usr/bin/dump
COPY scripts/entrypoint.sh /bin/entrypoint
COPY scripts/dumpCron.sh /bin/dumpCron
COPY scripts/functions.sh /opt/functions
RUN chmod +x /usr/bin/dump /bin/entrypoint /bin/dumpCron

RUN mkdir -p /dumps
RUN chown -R 1000:1000 /dumps
RUN chmod 440 -R /dumps
RUN touch /var/log/cron.log

run mkdir -p /root/.ssh
run echo "IdentityFile /root/.ssh/privateKey" > /root/.ssh/config
run echo "    StrictHostKeyChecking no" >> /etc/ssh/ssh_config

ENTRYPOINT ["/bin/entrypoint"]
