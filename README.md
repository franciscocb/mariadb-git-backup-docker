This project intends to use a git repository as a database dump backup as a proof of concept.

## Environment

This project uses environment variables to determine connection options and git bot information.

- `MYSQL_DATABASE` the name of the database you want to backup
- `MYSQL_USER`: the username which will access the database
- `MYSQL_PASSWORD`: `MYSQL_USERNAME` corresponding password
- `MYSQL_HOST`: hostname or IP address of the host hosting the database
- `GIT_USER`: `user.name` option in `git config`
- `GIT_EMAIL`: `user.email` option in `git config`

## Volumes

This container provides two volumes:

- `/dumps` is where this container will generate a git repository, store dumps and push them into a remote. **You should mount this volume if you want to keep you data persistent locally**.
- `/privateKey` is where you should mount your SSH private key in order to be able to push updates into your remote repository.

## Backup to git remote

In order to use this feature, you need to do as follows:

- Access `/dumps` by either
  - mounting to some local directory (recommended)
  - accessing the container directory
- Adding the `origin` remote manually, using the command `git remote add origin [origin url]`

Now, the container will push automatically to you remote repository.

## Example `docker-compose.yml`

```yml
version: "3"

services:
  services:
  db:
    image: mariadb:latest
    ports:
      - 127.0.0.1:3306:3306
    environment:
      MYSQL_ROOT_PASSWORD: root
      MYSQL_DATABASE: api

  backup:
    image: registry.gitlab.com/franciscocb/mariadb-git-backup-docker:latest
    depends_on:
      - db
    volumes:
      - ./dumps:/dumps
      - ~/.ssh/id_rsa:/privateKey
    environment:
      MYSQL_DATABASE: api
      MYSQL_USER: root
      MYSQL_PASSWORD: root
      MYSQL_HOST: db
      GIT_USER: bot
      GIT_EMAIL: bot@test

  # ... other services
```

> I don't hold any responsability on any data loss. Please use a correct and recommended way of backup your data.
